const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for create order
router.post("/checkout",auth.verify,  (req,res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		products: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	userController.checkout(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieve user details
router.get("/:userId/userDetails", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization).id;
	userController.userDetails(req.params.userId, userData).then(resultFromController => res.send(resultFromController));
});


module.exports = router;