const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");




// Route for creating a product
router.post("", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the courses
router.get("/all", (req, res) => {

	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the ACTIVE products
router.get("", (req, res) => {

	productController.activeProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a single product
router.get("/:productId", (req, res) => {

	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product information
router.put("/:productId", auth.verify, (req, res) => {

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for archive a product
router.put("/:productId/archive",auth.verify,(req,res)=>{

    productController.archiveProduct(req.params,req.body).then(resultFromController => res.send(
        resultFromController));
});

// Route for activate a product
router.put("/:productId/activate",auth.verify,(req,res)=>{

    productController.activateProduct(req.params,req.body).then(resultFromController => res.send(
        resultFromController));
});

module.exports = router;