const Product = require("../models/Product");

module.exports.createProduct = (data) => {

	console.log(data);

	if(data.isAdmin){

		// Creates a variable "newProduct" and instantiates a new "Product" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		// Saves the created object to our database
		return newProduct.save().then((product, error) => {

			// Product creation failed
			if(error){

				return false;

			// Product creation successful
			} else {

				return true;
			}
		})


	}

	// Since Promise.resolve() returns a resolved promise, the variable message will already be in a resolved status.
	let message = Promise.resolve("User must be an Admin to access this!");
	return message.then((value) => {
		return value
	});
	
};

module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {
		return result;
	});
};

module.exports.activeProducts = () => {

	return Product.find({ isActive: true }).then(result => {
		return result;
	});
};

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};

module.exports.archiveProduct =(reqParams,reqBody) =>{
    let archivedProduct ={
        isActive : false
    };

    return Product.findByIdAndUpdate(reqParams.productId,archivedProduct).then((product, error)=>{
        if(error){
            return false;
        }else{
            return true;
        };
    });
};

module.exports.activateProduct =(reqParams,reqBody) =>{
    let activateProduct ={
        isActive : true
    };

    return Product.findByIdAndUpdate(reqParams.productId,activateProduct).then((product, error)=>{
        if(error){
            return false;
        }else{
            return true;
        };
    });
};