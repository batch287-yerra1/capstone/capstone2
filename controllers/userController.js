const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (reqBody) => {

	console.log(reqBody);

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin,
		
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		console.log(result);

		if(result == null){

			return false;
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

				return {access : auth.createAccessToken(result)}
			
			} else {

				return false
			};
		};
	});
};


module.exports.checkout = async (data) => {

	let sumAll = 0;
    for (let i = 0; i < data.products.length; i++) {
        let productPrice =await Product.findById(data.products[i].Id).then(result =>{
        	data.products[i].productId = data.products[i].Id;
        	delete data.products[i].Id;
        	data.products[i].productName = result.name;
        	return result.price;
       	 });
        sumAll += productPrice * data.products[i].quantity;
    }

    console.log(data);

    if(!data.isAdmin){
    	let isUserUpdated = await User.findById(data.userId).then(user => {user.orderedProduct.push({ products: data.products, totalAmount : sumAll});

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});
    	let isProductUpdated = [];

    	 for (let i = 0; i < data.products.length; i++) {
			isProductUpdated.push(await Product.findById(data.products[i].productId).then(product => {product.userOrders.push({userId: data.userId});

			return product.save().then((course, error) => {
				if(error){
					return false;
				} else {
					return true;
				};
			});
		}));
	}	
    
    let isPUpdated = true;

    for (let i = 0; i < data.products.length; i++) {

    	isPUpdated = isPUpdated && isProductUpdated[i];
    }

    if(isUserUpdated && isPUpdated){
		return true;
	} else {
		return false;
	};
}
}

module.exports.userDetails = (reqParams, userData)=> {

	let updatedUser = {
		"password" : ""
	};

	let message = Promise.resolve("User must be an Admin to access this!");

	console.log(reqParams, userData, reqParams == userData);
	if(reqParams == userData){
		message = Promise.resolve(User.findByIdAndUpdate(reqParams,updatedUser).then((user, error)=>{
			console.log(user);
	        if(error){
	            return false;
	        }else{
	            return user;
	        };
	    }));
	} 

	return message.then((value) => {
		return value
	});


	// return User.findByIdAndUpdate(reqParams,updatedUser).then((user, error)=>{
	// 	if(reqParams == userData){
	// 		console.log(user);
	//         if(error){
	//             return false;
	//         }else{
	//             return user;
	//         };
	// 	} else {
	// 		return `can't found Id`
	// 	}
	// 	});	
};

