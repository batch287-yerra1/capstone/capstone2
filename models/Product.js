const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, "Name is required."]
	},
	description:{
		type: String,
		required: [true, "Description is required."]
	},
	price:{
		type: String,
		required: [true, "Price is required."]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type:Date,
			// The "new Data()" expression instantiates a new "date" taht store the current date and time whenever a course is created inour databse
		default: new Date()
	},
	userOrders: [
			{
				userId: {
					type: String,
					required: [true, "User id is required."]
				}
			}
		]
})

module.exports = mongoose.model("Product", productSchema);